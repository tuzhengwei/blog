package com.tuzw.controller;

import com.tuzw.Result;
import com.tuzw.api.TestService;
import com.tuzw.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/11 0011 11:30
 */
@RestController
public class HelloCtroller {

    @Autowired
    private TestService testService;

    @RequestMapping("/test")
    public Result test() {
        /**
         * 自定义异常测试
         * int i = 10/0;  访问：http://localhost:8080/test，报错：自定义异常
         */
        /**
         * 自定义运行时异常测试
         * throw new MyRunException("400", "自定义运行时异常测试");
         */
        String result = testService.test();
        if(result != null){
            return Result.success().codeAndMessage("200", "成功");
        }
        return Result.error().codeAndMessage("404","没有找到资源");
    }

    @RequestMapping("/select")
    public Result query(){
        User user = testService.getUserById(1);
        Result result = Result.success();
        result.setData(user);
        return result;
    }
}
