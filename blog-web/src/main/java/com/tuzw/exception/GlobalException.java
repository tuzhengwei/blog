package com.tuzw.exception;

import com.tuzw.Result;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理类
 * tuzw
 * @version 1.0
 * @date 2021/10/13 0013 14:48
 */
@ControllerAdvice // 作用是给Controller控制器添加统一的操作或处理
public class GlobalException {

    @ExceptionHandler(Exception.class)
    @ResponseBody
    public Result myException(Exception e){
        e.printStackTrace();
        return Result.error().codeAndMessage("400", "自定义异常");
    }

    @ExceptionHandler(MyRunException.class)
    @ResponseBody
    public Result myRunException(MyRunException e){
        e.printStackTrace();
        return Result.error().codeAndMessage(e.getCode(), e.getMessage());
    }
}
