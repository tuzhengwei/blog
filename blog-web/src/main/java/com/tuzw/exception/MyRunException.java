package com.tuzw.exception;

/**
 * 运行时异常
 * @author tuzw
 * @version 1.0
 * @date 2021/10/13 0013 15:00
 */
public class MyRunException extends RuntimeException {

    /**
     * 状态码
     */
    private String code;
    /**
     * 消息
     */
    private String message;

    public MyRunException(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
