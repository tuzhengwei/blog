package com.tuzw;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/11 0011 12:02
 */
@SpringBootApplication
@MapperScan("mapper")
public class SpringBootApplicationMain {
    public static void main(String[] args) {
        SpringApplication.run(SpringBootApplicationMain.class, args);
    }
}
