package com.tuzw.Service;

import com.tuzw.api.TestService;
import com.tuzw.entity.User;
import com.tuzw.mapper.TestMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/11 0011 11:43
 */
@Service
public class TestServiceImpl implements TestService {
    public String test() {
        return "Hello world";
    }

    /**
     * No beans of 'TestMapper' type found
     * 此报错不会影响程序运行
     */
    @Autowired
    private TestMapper testMapper;

    @Override
    public User getUserById(Integer id) {
        return testMapper.getUserById(id);
    }
}
