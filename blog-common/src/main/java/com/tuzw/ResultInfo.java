package com.tuzw;

import lombok.Getter;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/13 0013 14:23
 */
public enum ResultInfo {

    /**
     * 返回的示例对象
     */
    NOT_FOUND("404", "资源没有找到");


    @Getter
    private String code;
    @Getter
    private String msg;

    ResultInfo(String code, String msg) {
        this.code = code;
        this.msg = msg;
    }
}