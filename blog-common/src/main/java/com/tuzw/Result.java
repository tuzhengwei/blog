package com.tuzw;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.util.HashMap;
import java.util.Map;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/11 0011 16:22
 */
public class Result {
    /**
     * 返回：状态成功 或者 失败
     */
    private boolean status;
    /**
     * 返回 状态码
     */
    private String code;
    /**
     * 返回信息
     */
    private String message;
    /**
     * 返回数据
     */
//    Map<String, Object> map = new HashMap<>();
    private Object data;

    public  Result(){

    }
    public Result(Boolean status, String message, String code){
        this.status = status;
        this.message = message;
        this.code = code;
    }
    public Result(Boolean status, String message){
        this.status = status;
        this.message = message;
    }
    /**
     * 获取成功的Result对象
     * @return
     */
    public static  Result success(){
        Result result = new Result();
        result.status = true;
        return result;
    }
    /**
     * 获取失败的Result对象
     * @return
     */
    public static  Result error(){
        Result result = new Result();
        result.status = false;
        return result;
    }
    /**
     * 设置错误码
     * @param code
     * @return Result
     */
    public Result code(String code){
        this.setCode(code);
        return this;
    }
    /**
     * 设置消息
     * @param message
     * @return
     */
    public Result message(String message){
        this.setMessage(message);
        return this;
    }
    /**
     * 设置错误码和消息
     * @param code
     * @param message
     * @return
     */
    public Result codeAndMessage(String code, String message){
        this.setCode(code);
        this.setMessage(message);
        return this;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
