package com.tuzw.api;

import com.tuzw.entity.User;

/**
 * @author tuzw
 * @version 1.0
 * @date 2021/10/11 0011 11:42
 */
public interface TestService {
    String test();

    User getUserById(Integer id);
}
